#!/bin/bash

if [ -z "${AWS_ACCESS_KEY_ID}" ]; then
    printf "Please provide a AWS ACCESS KEY ID for login to ECR";
    exit 1;
fi

if [ -z "${AWS_SECRET_ACCESS_KEY}" ]; then
    printf "Please provide a AWS SECRET ACCESS KEY for login to ECR";
    exit 1;
fi

if [ -z "${AWS_DEFAULT_REGION}" ]; then
    printf "Please provide a AWS DEFAULT REGION used in AWS CLI";
    exit 1;
fi

if [ -z "${BITBUCKET_REPO_SLUG}" ]; then
    printf "BITCBUCKET REPO SLUG is not defined please provide to set as Docker registry in ECS";
    exit 1;
fi

if [ -z "${BITBUCKET_BUILD_NUMBER}" ]; then
    printf "BITBUCKET_BUILD_NUMBER not defined please provide for taggging Docker image";
    exit 1;
fi

if [ -z "${APP_TAG_PREFIX}" ]; then
    printf "ERROR: Tag Prefix is not defined, Please set environment variable Application Tag Prefix."
    exit 1
fi

if [ -z "$1" ]; then
    printf "Artifacts path is missing please pass as Argument";
    exit 1;
fi  

printf "Installing AWS CLI >>>>>>>>>>>>>>>>>>>>"
apt-get update && apt-get install -y python-pip libpython-dev
pip install awscli

printf "Set AWS_ACCOUNT_ID"
export AWS_ACCOUNT_ID=$(aws sts get-caller-identity --output text --query Account) 

printf "Setting Registry Name.."
export ECR_REGISTRY_NAME=$AWS_ACCOUNT_ID".dkr.ecr."$AWS_DEFAULT_REGION".amazonaws.com/"$BITBUCKET_REPO_SLUG

printf "Logging to AWS ECR >>>>>>>>>>>>>>>>>>>>"
if eval $(aws ecr get-login --no-include-email); then
    printf "Login to ECR successfull.\n"
    if aws ecr describe-repositories  --repository-name $BITBUCKET_REPO_SLUG; then
        printf "${BITBUCKET_REPO_SLUG} Repo already exists.\n"
    else
        printf "${BITBUCKET_REPO_SLUG} Repo doesn't exists.\n"
        if aws ecr create-repository  --repository-name $BITBUCKET_REPO_SLUG; then
            printf "${BITBUCKET_REPO_SLUG} Repo created successfully.\n"
        else
            printf "${BITBUCKET_REPO_SLUG} Repo not created.\n"
            exit 1;
        fi
    fi
    printf "Build Docker Image >>>>>>>>>>>>>>>>>>>>"
    
    printf "Navigate to Internal-Artifacts DIR"
    cd $1

    if docker build -f Dockerfile -t $BITBUCKET_REPO_SLUG:${APP_TAG_PREFIX}.${BITBUCKET_BUILD_NUMBER} .; then
        printf "Docker image build successfully";
        printf "Tag Docker Image >>>>>>>>>>>>>>>>>>>>"
        if docker tag $BITBUCKET_REPO_SLUG:${APP_TAG_PREFIX}.${BITBUCKET_BUILD_NUMBER} $ECR_REGISTRY_NAME:${APP_TAG_PREFIX}.${BITBUCKET_BUILD_NUMBER}; then
            printf "Docker image tagged successfully";
            printf "Push Docker Image >>>>>>>>>>>>>>>>>>>>"
            if docker push $ECR_REGISTRY_NAME:${APP_TAG_PREFIX}.${BITBUCKET_BUILD_NUMBER}; then
                printf "Docker image["$ECR_REGISTRY_NAME:${APP_TAG_PREFIX}.${BITBUCKET_BUILD_NUMBER}"] pushed successfully.";
            else
                printf "Docker image push failed";
                exit 1;
            fi
        else
            printf "Docker image tag failed";
            exit 1;
        fi
    else
        printf "Docker image build failed";
        exit 1;
    fi
else
    printf "Not able to login to ECR."
    exit 1;    
fi