# Take microsoft aspnet core image 
FROM        node:8.9.4

# Set working directory
WORKDIR     /app

# Copy the artefacts
COPY        . ./

# Port where your app will run
EXPOSE      3000

# Entry point that will start the Microservice in Docker Container
ENTRYPOINT  ["npm", "start"]