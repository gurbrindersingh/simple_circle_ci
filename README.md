# Node-Microservice

This template is designed to help developers to fast tract creation of service. It also mandates important building blocks for the service to be defined and maintained.

# Node MicroService

Template for Node Microservice

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them

```
Node@v8.9.4
PostgreSql
```
### Installing

A step by step series that will tell you how to get a development env running

```
$ cd code
```

```
$ npm install
```

## For Validating Modules

```
$ npm run nsp:check
```
## For linting

```
$ npm run lint
```

## Run the Server

* For Development
```
$ npm run start:dev
```
* For Production
```
$ npm run start:prod
```
## Run the test cases

```
$ npm run test
```