/**
 * ping method to check
 * whether microservice is working
 * @return {object} status - returns ok that specifies that microservice is working.
 * */
const ping = (req, res) => {
  return res.status(200).json({ status: 'ok' });
};

module.exports = {
  ping,
};
