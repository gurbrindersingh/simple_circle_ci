const pingController = require('./ping');
const healthController = require('./healthcheck');

module.exports = {
  ...pingController,
  ...healthController,
};
