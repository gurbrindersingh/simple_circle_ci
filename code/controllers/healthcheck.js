const database = require('../database');

/**
 * healthcheck method
 * to test database connectivity
 * @return {json} status - 200/500
 * @return {json} msg - description for testing the database connection to microservice.
 * */
const healthcheck = async (req, res) => {
  try {
    await database.authenticate();

    return res.status(200).json({ status: 200, msg: 'Connected with database successfully' });
  } catch (err) {
    return res.status(500).json({ status: 500, msg: 'Exception raised while connecting to database.' });
  }
};

module.exports = {
  healthcheck,
};
