const controller = require('../controllers');

module.exports = (router) => {
  router.route('/ping').get(controller.ping);

  return router;
};
