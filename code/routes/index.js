const ping = require('./ping');
const healthcheck = require('./healthcheck');

module.exports = (router) => {
  ping(router);
  healthcheck(router);

  return router;
};
