const controller = require('../controllers');

module.exports = (router) => {
  router.route('/healthcheck').get(controller.healthcheck);

  return router;
};
