const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

const { expect } = chai;
chai.use(chaiHttp);

/**
 * Test the /GET route for health api
 * */
context('Test cases for healthcheck', () => {
  describe('/GET', () => {
    it('database connected successfully', (done) => {
      chai.request(server).get('/healthcheck').end((err, res) => {
        expect(err).to.equal(null);
        expect(res.status).to.equal(200);
        expect(res.body).to.deep.equal({ status: 200, msg: 'Connected with database successfully' });

        done();
      });
    });
  });
});

