const chai = require('chai');
const chaiHttp = require('chai-http');
const server = require('../server');

const { expect } = chai;
chai.use(chaiHttp);

/**
 * Test the /GET route
 * */
context('Test cases for ping', () => {
  describe('/GET', () => {
    it('microservice is working', (done) => {
      chai.request(server).get('/ping').end((err, res) => {
        expect(res.status).to.equal(200);
        expect(res.body).to.deep.equal({ status: 'ok' });
        done();
      });
    });
  });
});
