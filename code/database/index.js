const Sequelize = require('sequelize');

/**
 * method to initialize db connection
 * @param {string} dbName - database name we are using for this microservice.
 * @param {string} dbUser - database username.
 * @param {string} dbPassword - set database password.
 * @param {string} dbPort - port (eg: 5432).
 * @param {string} dbHost - hostname (eg: localhost).
 * @param {string} dbDialect - 'postgres'.
 * @return {boolean} true.
 * */
function init() {
  try {
    this.sequelize = new Sequelize(process.env.POSTGRES_CONNECTIONSTRING);
  } catch (e) {
    throw new Error('Exception raised while connecting to database....');
  }
}

/**
 * method to test the connection
 * @return {object} conncetion sequelize object
 * */
function authenticate() {
  return this.sequelize.authenticate();
}

module.exports = {
  init,
  authenticate,
};
