module.exports = {
    "extends": "airbnb-base",
    "globals": {
      "it": true,
      "context": true,
      "describe": true,
    },
    "rules":{
      "no-multiple-empty-lines": ['error', { max: 1, maxEOF: 1, maxBOF: 0 }],
      "comma-spacing": ['error', {"before": false, "after": true}],
      "newline-after-var": 0,
      "eol-last": 1,
      "import/no-extraneous-dependencies": ["error", {"devDependencies": true}],
      "arrow-body-style": ["error", "always"],
      "newline-before-return": 2,
    }
};
