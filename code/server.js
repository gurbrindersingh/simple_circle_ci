/**
 * Declarations of dependencies
 * */
const express = require('express');
const fs = require('fs');
const path = require('path');
const cors = require('cors');
const compression = require('compression');
const helmet = require('helmet');
const morgan = require('morgan');
const config = require('./config');
const db = require('./database');
const router = require('./routes')(express.Router());

const app = express();
const dir = './logs';

if (!fs.existsSync(dir)) fs.mkdirSync(dir);

/**
 * list of all Middlewares used in project
 * cors, compression, helmet
 * */
app.use(cors());
app.use(compression());
app.use(helmet());
app.use('/', router);
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

/**
 * generating access.log
 * to generate the logs using morgan
 * logs are generated in access.logs file inside logs folder
 * */
const accessLogStream = fs.createWriteStream(path.resolve(process.cwd(), 'logs', 'access.log'), { flags: 'a' });
app.use(morgan('combined', { stream: accessLogStream }));

/**
 * Database initialisation
 * Creating connection between Microservice and database
 * */
db.init();

/**
 * Start the app by listening <port>
 * */
app.listen(config.port);

module.exports = app;
